package com.ui.factory;

import com.common.CommandExtractor;
import com.common.ExtractedCommand;
import com.ui.command.*;

public class CommandFactory {
    public static Command create(String command) {

        ExtractedCommand extractedCommand = CommandExtractor.extract(command);

        switch (extractedCommand.getName()) {
            case QuitCommand.COMMAND_NAME:
                return new QuitCommand();
            case AreaStategyCommand.COMMAND_NAME:
                return new AreaStategyCommand(extractedCommand.getArgs());
            case ShowAllCommand.COMMAND_NAME:
                return new ShowAllCommand(extractedCommand.getArgs());
            case HelloCommand.COMMAND_NAME:
                return new HelloCommand();
        }
        throw new CommandNotFoundException(extractedCommand.getName());
    }
}
