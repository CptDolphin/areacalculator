package com.ui.command;

import com.ui.CommandResult;

public class QuitCommand implements Command{
    public static final String COMMAND_NAME = "quit";

    @Override
    public CommandResult execute() {
        return new CommandResult("Bye bye");
    }

    @Override
    public String getCommandName() {
        return COMMAND_NAME;
    }
}
