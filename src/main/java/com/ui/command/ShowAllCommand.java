package com.ui.command;

import com.calc.registry.report.DottedReport;
import com.calc.registry.report.Report;
import com.calc.registry.report.ReportFactory;
import com.common.CommandExtractor;
import com.common.ExtractedCommand;
import com.ui.CommandResult;

public class ShowAllCommand implements Command {
    public static final String COMMAND_NAME = "showall";

    private String[] args;

    public ShowAllCommand(String[] args) {
        this.args = args;
    }

    @Override
    public CommandResult execute() {
        ExtractedCommand extractedCommand = (args.length == 0)
                ? new ExtractedCommand(DottedReport.REPORT_NAME,args)
                : CommandExtractor.extract(args);

        Report report = ReportFactory.create(extractedCommand.getName());
        return new CommandResult(report.generateReport());
    }

    @Override
    public String getCommandName() {
        return COMMAND_NAME;
    }
}
