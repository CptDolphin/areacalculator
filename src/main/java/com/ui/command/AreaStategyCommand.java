package com.ui.command;

import com.calc.factory.AreaOperationFactory;
import com.calc.policy.CalculationPolicy;
import com.calc.registry.ReportDbSingleton;
import com.common.Validator;
import com.ui.CommandResult;

public class AreaStategyCommand implements Command {
    public static final String COMMAND_NAME = "area";
    public static final String SHOULD_HAVE_ARGUMENTS = "Area command should have arguments";
    private final String[] args;

    public AreaStategyCommand(String[] args) {
        this.args = args;
    }

    @Override
    public CommandResult execute() {
        Validator.isNotEmpty(args,SHOULD_HAVE_ARGUMENTS);
        CalculationPolicy calculationPolicy = new AreaOperationFactory().create(args);

        double result = calculationPolicy.calculate();

        CommandResult comandResult = new CommandResult(String.valueOf(result));
        ReportDbSingleton.SINGLETON.addCommand(getCommandName(),comandResult,args);
        return comandResult;

    }

    @Override
    public String getCommandName() {
        return COMMAND_NAME;
    }
}
