package com.ui.command;

import com.ui.CommandResult;

public interface Command {
    CommandResult execute();

    String getCommandName();
}
