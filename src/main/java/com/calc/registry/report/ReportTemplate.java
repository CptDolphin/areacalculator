package com.calc.registry.report;

import com.calc.registry.OperationInfo;

import java.util.List;

public class ReportTemplate {
    public static final String NEW_LINE = "\n";
    public static final String SPACE = " ";

    public String createReport(List<OperationInfo> operations, String delimiter) {
        if (operations.size() == 0) {
            return "List of operations is empty";
        } else {
            String raport = generateHeader(operations.get(0), delimiter);
            for (OperationInfo operationInfo : operations) {
                raport += generateRow(operationInfo, delimiter);
            }
            return raport;
        }
    }

    private String generateRow(OperationInfo operationInfo, String delimiter) {
        return delimiter
                + SPACE + operationInfo.getCommand()
                + SPACE + delimiter + delimiter
                + SPACE + operationInfo.getResult() + NEW_LINE;
    }

    private String generateHeader(OperationInfo operationInfo, String delimiter) {
        String firstLine = new StringBuilder()
                .append("Lista operacja od ")
                .append(operationInfo.getExecutionTime())
                .append(NEW_LINE)
                .toString();
        return firstLine + generateHeaderLine(firstLine.length(), delimiter);
    }

    private String generateHeaderLine(int length, String delimiter) {
        String result = "";
        for (int i = 0; i < length; i++) {
            result += delimiter;
        }
        return result + NEW_LINE;
    }
}
