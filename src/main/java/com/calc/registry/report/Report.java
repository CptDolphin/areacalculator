package com.calc.registry.report;

public interface Report {
    String generateReport();
}
