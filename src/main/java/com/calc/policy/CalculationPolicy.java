package com.calc.policy;

public interface CalculationPolicy {
    double calculate();
}
