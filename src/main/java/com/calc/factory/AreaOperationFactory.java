package com.calc.factory;

import com.calc.policy.CalculationPolicy;
import com.calc.policy.RectanglePolicy;
import com.calc.policy.SquarePolicy;
import com.common.CommandExtractor;
import com.common.ExtractedCommand;

public class AreaOperationFactory {
    public static CalculationPolicy create(String[] args) {
        ExtractedCommand extractedCommand = CommandExtractor.extract(args);
        switch (extractedCommand.getName()) {
            case "square":
                return new SquarePolicy(extractedCommand.getArgs());
            case "rectangle":
                return new RectanglePolicy(extractedCommand.getArgs());
        }
        throw new UnknownFigureException("Calculation are for figure :" + extractedCommand.getName() + " is not implemented.");
    }
}
